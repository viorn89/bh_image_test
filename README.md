1. Клонируем репозиторий, проверям что бы в операционной системе был curl (в linux и osx есть из коробки, в windows не знаю, но думаю тоже)

2. переходим в папку со скриптом, запускаем sh test.sh

```
vladimiryuyukin@MacBook-Pro-Vladimir bh_image_test % sh test.sh                                                    
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  119k  100  119k    0     0  86745      0  0:00:01  0:00:01 --:--:-- 86684
time_namelookup:  0,002131
       time_connect:  0,176392
    time_appconnect:  0,664645
   time_pretransfer:  0,664723
      time_redirect:  0,000000
 time_starttransfer:  0,886045
                    ----------
         time_total:  1,413267
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  119k  100  119k    0     0  90592      0  0:00:01  0:00:01 --:--:-- 90592
time_namelookup:  0,002143
       time_connect:  0,174479
    time_appconnect:  0,611963
   time_pretransfer:  0,612023
      time_redirect:  0,000000
 time_starttransfer:  0,830271
                    ----------
         time_total:  1,353961

```

я проверил на большом количестве фоток, в среднем значение валирует от 1.2 до 1.7 сек
но встречались и 3-4 сек, в любом случае для файлов в 100-200кб это слишком много.

Скорее всего дело в расположении сервер, трассировка выдает не хороший результат

```
traceroute api.behappy2day.com
traceroute to behappy2day.com (69.162.106.243), 64 hops max, 52 byte packets
 1  192.168.1.1 (192.168.1.1)  2.957 ms  1.766 ms  1.750 ms
 2  10.255.61.254 (10.255.61.254)  3.762 ms  4.489 ms  4.919 ms
 3  10.255.61.6 (10.255.61.6)  3.223 ms  4.212 ms  3.235 ms
 4  10.255.61.10 (10.255.61.10)  3.424 ms  3.372 ms  4.896 ms
 5  kai-cr01-ae23.200.rnd.mts-internet.net (195.34.36.114)  3.660 ms  4.254 ms  3.374 ms
 6  a433-cr02-be3.61.msk.mts-internet.net (212.188.28.133)  18.184 ms * *
 7  a197-cr04-be2.77.msk.mts-internet.net (212.188.28.149)  19.069 ms  20.056 ms  19.523 ms
 8  * anc-cr03-ae3.77.ff.mts-internet.net (195.34.59.50)  51.051 ms *
 9  xe-1-2-0.mpr1.fra4.de.above.net (80.81.194.26)  51.564 ms * *
10  * * *
11  ae2.cs1.ams17.nl.eth.zayo.com (64.125.29.59)  171.769 ms * *
12  ae4.cs3.ams10.nl.eth.zayo.com (64.125.28.37)  173.832 ms * *
13  ae10.cs1.lhr15.uk.eth.zayo.com (64.125.29.17)  171.352 ms * *
14  * * *
15  * * *
16  * * ae23.cs3.iad93.us.eth.zayo.com (64.125.28.189)  173.160 ms
17  ae8.cs1.dca2.us.eth.zayo.com (64.125.25.98)  171.598 ms  170.788 ms *
18  ae3.cs1.iah1.us.eth.zayo.com (64.125.29.49)  173.018 ms  171.734 ms  172.370 ms
19  ae5.cs1.dfw2.us.eth.zayo.com (64.125.28.99)  171.460 ms  170.993 ms  176.762 ms
20  ae11.mpr1.dfw15.us.zip.zayo.com (64.125.21.5)  170.049 ms  172.387 ms  171.779 ms
21  64.125.41.226.ipyx-236370-900-zyo.zip.zayo.com (64.125.41.226)  171.421 ms  171.709 ms  171.374 ms
22  * * *
23  te6-1.bdr2.core1.dllstx3.dallas-idc.com (208.115.192.58)  172.072 ms
    te5-1.bdr1.core1.dllstx3.dallas-idc.com (208.115.192.50)  268.764 ms
    te6-1.bdr2.core1.dllstx3.dallas-idc.com (208.115.192.58)  172.507 ms
24  ge0-46.vl370.cr02-119.dllstx3.dallas-idc.com (64.31.3.50)  171.905 ms  171.821 ms  171.257 ms
25  * * *
26  * * *
27  * * *
28  * * *
29  * * *
30  * * *
31  * * *
32  *
```

